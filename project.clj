(defproject refrigelist "0.1"
  :description "Simple refrigerator content manager"
  :url "https://bitbucker.org/eithz/refrigelist"
  :license {:name "GNU General Public License"
            :url "http://www.gnu.org/licenses/gpl.html"}
  :jvm-opts ["-XX:MaxPermSize=256m"]
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [compojure "1.3.4"]
                 [quiescent "0.2.0-RC2"]
                 [http-kit "2.1.18"]
                 [org.clojure/clojurescript "0.0-3308"]]
  :profiles {:dev {:plugins [
                             [lein-cljsbuild "1.0.6"]
                             [lein-figwheel "0.3.7"]
                             ]
                   :dependencies [[reloaded.repl "0.1.0"]]
                   :source-paths ["dev"]}}
  :cljsbuild {:builds [{:source-paths ["src" "dev"]
                        :figwheel true
                        :compiler {:output-to "target/classes/public/app.js"
                                   :output-dir "target/classes/public/out"
                                   :optimizations :none
                                   :asset-path "/out"
                                   :main "refrigelist.client"
                                   :recompile-dependents true
                                   :source-map true}}]})
