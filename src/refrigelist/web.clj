(ns refrigelist.web
  (:require [compojure.core :refer [defroutes GET]]
           [compojure.route :refer [resources]]))


(defroutes app
  (resources "/"))
