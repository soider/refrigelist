(ns refrigelist.client
  (:require [refrigelist.data :as data]
            [refrigelist.components :refer [render]]
            [cljs.core.async :as a])
  (:require-macros [cljs.core.async.macros :as am]))


(defn load-app
  "Return a map containing initial application"
  []
  {
   :state (atom (or (data/load-data) (data/new-data)))
   :channels
   {
    :test (a/chan)
    :delete (a/chan)
    :increase (a/chan)
    :decrease (a/chan)
    }
   :consumers
   {
    :test (fn [state]
            (assoc-in state [0 :quantity] 10))
    :delete data/delete-product
    :increase data/increase-quantity
    :decrease data/decrease-quantity
    }})


(defn init-updates
  "For every entry in a map of channel identifiers to consumers,
  initiate a channel listener which will update the application state
  using the appropriate function whenever a value is recieved, as
  well as triggering a render."
  [app]
  (doseq [[ch update-fn] (:consumers app)]
    (am/go (while true
             (let [val (a/<! (get (:channels app) ch))
                   _ (.log js/console (str "on channel [" ch "], recieved value [" val "]"))
                   new-state (swap! (:state app) update-fn val)]
               (.log js/console (str "SWAPPED " new-state))
               (render app))))))

(defn ^:export main
  "Application entry point"
  []
  (let [app (load-app)]
    (init-updates app)
    (render app)))

(main)
