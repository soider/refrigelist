(ns refrigelist.data)

(defn load-data [])

(defn new-data []
  [
                   {:title "Milk"
                    :image {
                            :url "images/milk.jpeg"
                            }
                    :id 0
                    :quantity 0
                    :deleted false}

                   {:title "Milk"
                    :image {
                            :url "images/milk.jpeg"
                            }
                    :id 1
                    :quantity 25
                    :deleted false}
                   {:title "Milk"
                    :image {
                            :url "images/milk.jpeg"
                            }
                    :id 3
                    :quantity 50
                    :deleted false}
                   {:title "Milk"
                    :image {
                            :url "images/milk.jpeg"
                            }
                    :id 4
                    :quantity 75
                    :deleted false}])


(defn delete-product [state id]
  (filterv (fn [product] (not (= (:id product) id))) state))
;;
;;(keep-indexed #(when (= (:id %2) 1) %1) data)

(def STEP 25)
(def LOWER-BOUND 0)
(def UPPER-BOUND 100)

(defn find-index-by-id [id data]
  (first (keep-indexed #(when (= (:id %2) id) %1) data)))

(defn increase-quantity [state id]
  (let [idx (find-index-by-id id state)
        _ (.log js/console "CURRENT STATE " state )
        current-value ((state idx) :quantity)]
    (.log js/console (str "CURRENT Q " current-value " for " id))
    (cond (< current-value UPPER-BOUND)
          (assoc-in state [idx :quantity ] (+ current-value STEP))
          true state
          )))

(defn decrease-quantity [state id]
  (let [idx (find-index-by-id id state)
        current-value ((state idx) :quantity)]
    (.log js/console (str "CURRENT Q " current-value " for " id))
    (cond (> current-value LOWER-BOUND)
          (assoc-in state [idx :quantity ] (- current-value STEP))
          true state
          )))



