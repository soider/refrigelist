(ns refrigelist.components
  (:require [quiescent.core :as q]
            [quiescent.dom :as d]
            [cljs.core.async :as a]
            )
  (:require-macros [cljs.core.async.macros :as am]) )

(q/defcomponent ProductRemoveButton [id channel]
  (d/button {:className "item__list__remove"
             :onClick (fn [e] (am/go (a/>! channel id)))}
            "remove"))

(q/defcomponent ProductTitle [title]
  (d/h3 {:clasName "item__list__element__title"}
        title))

(q/defcomponent ProductImage [image]
  (d/p {:className "item__list__element__image"}
       (d/img {:src (:url image)})))


(q/defcomponent ProductDecreaseButton [id channel]
  (d/button {:className "item__list__element__decrease"
             :onClick (fn [e]
                        (am/go (a/>! channel id))
                        )}
            "-"))


(q/defcomponent ProductIncreaseButton [id channel]
  (d/button {:className "item__list__element__increase"
             :onClick (fn [e]
                        (am/go (a/>! channel id))
                        )}
            "+"))

(defn detect-quantity-category [quantity]
  (cond (<= quantity 25) "little"
        (<= quantity 75) "medium"
        (<= quantity 100) "normal"))

(q/defcomponent ProductQuantityValue [quantity]
  (d/span {:className "item__element__quantity_value_total"}
          (d/span {:className
                   (str "item__element__quantity__value item__element__quantity__value__" (detect-quantity-category quantity))
                   :style {:width (str quantity "%")}
                   })))

(q/defcomponent ProductQuantityControl [[quantity id] channels]
  (d/span {:className "item__element__quantity__control"}
          (ProductDecreaseButton id (:decrease channels))
          (ProductQuantityValue quantity)
          (ProductIncreaseButton id (:increase channels))))

(q/defcomponent Product
  [{:keys [title quantity deleted image id]} channels]
  (.log js/console "Render product")
  (d/li {:className "item__list__element"}
        (ProductRemoveButton id (:delete channels))
        (ProductTitle (str title))
        (ProductImage image)
        (ProductQuantityControl  [quantity id] channels)))

(q/defcomponent PageTitle
  []
  (d/h1 {} "Refrigelist"))

(q/defcomponent ProductListControl
  [channels]
  (d/div {} "nothing here yet"))

(q/defcomponent Application
  [products channels]
  (d/div {}
         (PageTitle)
         (ProductListControl channels)
         (d/ul {:className "item__list"}
               (map #(Product % channels) products))))

(defn render [application]
  (q/render (Application @(:state application) (:channels application))
            (.getElementById js/document "main")))
