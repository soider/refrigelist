(ns user
  (:require [reloaded.repl :refer [system reset stop]]
            [refrigelist.system]))

(reloaded.repl/set-init! #'refrigelist.system/create-system)
